package edu.viniciussdsilva.trabalhofinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class IntegranteActivity extends AppCompatActivity {

    EditText etNmIntegrante, etNrMatricula;
    Button btnExcluir;
    Integer idGrupo, idIntegrante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_integrante);

        etNmIntegrante = findViewById(R.id.etNmIntegrante);
        etNrMatricula = findViewById(R.id.etNrMatricula);
        btnExcluir = findViewById(R.id.btnExcluir);

        Intent i = getIntent();
        idGrupo = i.getIntExtra("idGrupo", 1);
        idIntegrante = i.getIntExtra("idIntegrante", 0);
//        Toast.makeText(this, "id: " + idIntegrante, Toast.LENGTH_SHORT).show();
        if (idIntegrante > 0) {
            buscarIntegrante();
            btnExcluir.setVisibility(View.VISIBLE);
        }
    }

    public void finalizar(View view) {
        finish();
    }

    private void buscarIntegrante() {
        RequestParams params = new RequestParams("id", idIntegrante);
        WebServiceHandler.post("integrante", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    etNmIntegrante.setText(response.getString("nmIntegrante").toString());
                    etNrMatricula.setText(response.getString("nrMatricula").toString());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public void salvarIntegrante(View view) {
        if (!etNmIntegrante.getText().toString().equals("") && !etNrMatricula.getText().toString().equals("")) {
            RequestParams params = new RequestParams();
            params.put("idIntegrante", idIntegrante);
            params.put("idGrupo", idGrupo);
            params.put("nmIntegrante", etNmIntegrante.getText().toString());
            params.put("nrMatricula", etNrMatricula.getText().toString());

            WebServiceHandler.post("salvarIntegrante", params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Toast.makeText(getApplicationContext(), "Integrante cadastrado com sucesso!", Toast.LENGTH_SHORT).show();
                    finish();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toast.makeText(getApplicationContext(), responseString, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            colocaErroEditText(etNmIntegrante);
            colocaErroEditText(etNrMatricula);
        }

    }

    private void colocaErroEditText(EditText et) {
        if (et.getText().toString().equals("")) {
            et.setError("Campo obrigatório!");
        }
    }

    public void excluir(View view) {
        RequestParams params = new RequestParams("idIntegrante", idIntegrante);
        WebServiceHandler.post("excluirIntegrante", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Toast.makeText(getApplicationContext(), "Integrante excluído com sucesso!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
