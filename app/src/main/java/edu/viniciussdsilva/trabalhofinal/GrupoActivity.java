package edu.viniciussdsilva.trabalhofinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.loopj.android.http.*;

import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

public class GrupoActivity extends AppCompatActivity {

    ListView listaGrupos;
    List<Map<String, Object>> lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grupo);

        listaGrupos = findViewById(R.id.listaGrupos);
        listaGrupos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getApplicationContext(), "idGrupo: " + listaIntegrantes.get(i).get("idGrupo"), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), GrupoDetalheActivity.class);
                intent.putExtra("idGrupo", lista.get(i).get("idGrupo").toString());
                startActivity(intent);
            }
        });
        atualizaListaGrupos();
    }

    @Override
    protected void onResume() {
        super.onResume();
        atualizaListaGrupos();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        atualizaListaGrupos();
    }

    public void adicionarGrupo(View view) {

        WebServiceHandler.get("adicionaGrupo", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//                super.onSuccess(statusCode, headers, response);
                Toast.makeText(getApplicationContext(), "Grupo adicionado. Recarregando listaIntegrantes", Toast.LENGTH_SHORT).show();
                atualizaListaGrupos();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                super.onFailure(statusCode, headers, responseString, throwable);
                Toast.makeText(getApplicationContext(), "Erro ao adicionar grupo.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void atualizaListaGrupos() {

        WebServiceHandler.get("listaGrupos", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray array) {
                try {
                    lista = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        HashMap<String, Object> item = new HashMap<>();
                        String nmIntegrantes = "Nenhum integrante cadastrado", nmTema = "Nenhum tema cadastrado";

                        JSONObject grupo = (JSONObject) array.get(i);
                        JSONArray grupoCategoriaList = grupo.getJSONArray("grupoCategoriaList");
                        if (!grupo.getString("tema").equals("null")) {
                            JSONObject tema = grupo.getJSONObject("tema");
                            nmTema = tema.getString("nmTema");
                        }

                        JSONArray integranteList = grupo.getJSONArray("integranteList");
                        if (integranteList.length() > 0) {
                            nmIntegrantes = "";
                        }
                        for (int j = 0; j < integranteList.length(); j++) {
                            JSONObject integrante = (JSONObject) integranteList.get(j);
                            nmIntegrantes += integrante.getString("nmIntegrante");
                            if (j != integranteList.length()) {
                                nmIntegrantes += "\n";
                            }
                        }

                        Double media = 0.0;
                        for (int j = 0; j < grupoCategoriaList.length(); j++) {
                            JSONObject grupoCategoria = (JSONObject) grupoCategoriaList.get(j);

                            Double ntGrupoCategoria = (grupoCategoria.getString("ntGrupoCategoria").equals("null") ? 0.0 : grupoCategoria.getDouble("ntGrupoCategoria"));
                            media += ntGrupoCategoria;
                        }
                        media = media / grupoCategoriaList.length();
                        System.out.println("Média = " + media);

                        item.put("idGrupo", grupo.getInt("id"));
                        item.put("nmTema", nmTema);
                        item.put("ntGrupo", String.format("%.2f", media));
                        item.put("nmIntegrantes", nmIntegrantes);

                        lista.add(item);
                    }
                    SimpleAdapter adapter = new SimpleAdapter(
                            getApplicationContext(),
                            lista,
                            R.layout.lista_grupos_item,
                            new String[]{"idGrupo", "nmTema", "ntGrupo", "nmIntegrantes"},
                            new int[]{R.id.listaGruposIdGrupo, R.id.listaGruposNmTema, R.id.listaGruposNtGrupo, R.id.listaGruposNmIntegrantes});
                    listaGrupos.setAdapter(adapter);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}
