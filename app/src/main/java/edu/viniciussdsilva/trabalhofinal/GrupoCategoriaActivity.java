package edu.viniciussdsilva.trabalhofinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class GrupoCategoriaActivity extends AppCompatActivity {

    TextView tvNmGrupo, tvNmCategoria, tvDsCategoria;
    EditText etNtGrupoCategoria;
    Integer idGrupo, idGrupoCategoria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grupo_categoria);

        tvNmGrupo = findViewById(R.id.tvNmGrupo);
        tvNmCategoria = findViewById(R.id.tvNmCategoria);
        tvDsCategoria = findViewById(R.id.tvDsCategoria);
        etNtGrupoCategoria = findViewById(R.id.etNtGrupoCategoria);

        Intent i = getIntent();
        idGrupo = i.getIntExtra("idGrupo", 0);
        idGrupoCategoria = Integer.parseInt(i.getStringExtra("idGrupoCategoria").toString());

//        Toast.makeText(this, "idGrupo = " + idGrupo + "; idGrupoCategoria = " + idGrupoCategoria, Toast.LENGTH_SHORT).show();
        atualizaGrupoCategoria();
    }

    public void finalizar(View view) {
        finish();
    }

    public void salvarGrupoCategoria(View view) {
        RequestParams params = new RequestParams();
        params.put("idGrupoCategoria", idGrupoCategoria);
        params.put("ntGrupoCategoria", etNtGrupoCategoria.getText().toString());
        WebServiceHandler.post("salvarGrupoCategoria", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Toast.makeText(getApplicationContext(), "Nota salva com sucesso!", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), responseString, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void atualizaGrupoCategoria() {
        WebServiceHandler.get("grupoCategoria", new RequestParams("id", idGrupoCategoria), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject grupoCategoria) {
                try {

                    tvNmGrupo.setText("Grupo " + idGrupo);
                    JSONObject categoria = grupoCategoria.getJSONObject("categoria");
                    tvNmCategoria.setText(categoria.getString("nmCategoria"));
                    tvDsCategoria.setText(categoria.getString("dsCategoria"));
                    etNtGrupoCategoria.setText(grupoCategoria.getString("ntGrupoCategoria"));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

    }
}
