package edu.viniciussdsilva.trabalhofinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

public class GrupoDetalheActivity extends AppCompatActivity {

    NonScrollListView lvIntegrantes, lvCategorias;
    TextView grupoDetalheNmGrupo, tvAvaliacao, tvIntegrantes;
    Spinner spinner;
    List<Map<String, Object>> listaIntegrantes, listaCategorias, listaSpinner;
    Integer idGrupo, idTema;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grupo_detalhe);

        tvAvaliacao = findViewById(R.id.tvAvaliacao);
        tvIntegrantes = findViewById(R.id.tvIntegrantes);

        grupoDetalheNmGrupo = findViewById(R.id.grupoDetalheNmGrupo);
        lvIntegrantes = findViewById(R.id.lvIntegrantes);
        lvIntegrantes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getApplicationContext(), "idCarinha" + listaIntegrantes.get(i).get("idIntegrante"), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), IntegranteActivity.class);
                intent.putExtra("idIntegrante", Integer.parseInt(listaIntegrantes.get(i).get("idIntegrante").toString()));
                startActivity(intent);
            }
        });

        lvCategorias = findViewById(R.id.lvCategorias);
        lvCategorias.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getApplicationContext(), "idGrupoCategoria" + listaCategorias.get(i).get("idGrupoCategoria"), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), GrupoCategoriaActivity.class);
                intent.putExtra("idGrupo", idGrupo);
                intent.putExtra("idGrupoCategoria", listaCategorias.get(i).get("idGrupoCategoria").toString());
                startActivity(intent);
            }
        });

        spinner = findViewById(R.id.spinner);
        carregaItensSpinner();
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    idTema = Integer.parseInt(listaSpinner.get(i - 1).get("idTema").toString());
                } else {
                    idTema = null;
                }
//                Toast.makeText(getApplicationContext(), "idTema: " + idTema, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        Intent intent = getIntent();
        idGrupo = Integer.parseInt(intent.getStringExtra("idGrupo"));

        atualizaDadosGrupo();
    }

    @Override
    protected void onStart() {
        super.onStart();
        atualizaDadosGrupo();
    }

    @Override
    protected void onResume() {
        super.onResume();
        atualizaDadosGrupo();
    }

    public void adicionarIntegrante(View view) {
        Intent i = new Intent(this, IntegranteActivity.class);
        i.putExtra("idGrupo", idGrupo);
        startActivity(i);
    }

    public void atualizaDadosGrupo() {
        RequestParams params = new RequestParams();
        params.put("id", idGrupo);

        WebServiceHandler.get("grupo", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject grupo) {
                try {
                    JSONArray integranteList = grupo.getJSONArray("integranteList");
                    JSONArray grupoCategoriaList = grupo.getJSONArray("grupoCategoriaList");
                    grupoDetalheNmGrupo.setText("Grupo " + grupo.getString("id"));
                    listaIntegrantes = new ArrayList<>();
                    listaCategorias = new ArrayList<>();

                    if (!grupo.getString("tema").equals("null")) {
                        JSONObject tema = grupo.getJSONObject("tema");
                        Integer idTema = tema.getInt("id");
                        if (idTema != null && idTema > 0) {
                            for (int i = 0; i < listaSpinner.size(); i++) {
                                HashMap<String, Object> item = (HashMap<String, Object>) listaSpinner.get(i);
                                Integer idTemaAtual = Integer.parseInt(item.get("idTema").toString());
                                if (idTemaAtual == idTema) {
                                    spinner.setSelection(i + 1);
                                    break;
                                }
                            }
                        }
                    }

                    if (integranteList.length() > 0) {
                        for (int j = 0; j < integranteList.length(); j++) {
                            HashMap<String, Object> item = new HashMap<>();
                            JSONObject integrante = (JSONObject) integranteList.get(j);
                            item.put("idIntegrante", integrante.getString("id"));
                            item.put("nmIntegrante", integrante.getString("nmIntegrante"));
                            item.put("nrMatricula", integrante.getString("nrMatricula"));

                            listaIntegrantes.add(item);
                        }

                        SimpleAdapter adapterLvIntegrantes = new SimpleAdapter(
                                getApplicationContext(),
                                listaIntegrantes,
                                R.layout.lista_integrante_item,
                                new String[]{"idIntegrante", "nmIntegrante", "nrMatricula"},
                                new int[]{R.id.listaIntegranteIdIntegrante, R.id.listaIntegranteNmIntegrante, R.id.listaIntegranteNrMatricula});
                        lvIntegrantes.setAdapter(adapterLvIntegrantes);
                    } else {
                        tvIntegrantes.setVisibility(View.GONE);
                    }

                    if (grupoCategoriaList.length() > 0) {
                        Double media = 0.0;
                        for (int j = 0; j < grupoCategoriaList.length(); j++) {
                            HashMap<String, Object> item = new HashMap<>();

                            JSONObject grupoCategoria = (JSONObject) grupoCategoriaList.get(j);
                            JSONObject categoria = grupoCategoria.getJSONObject("categoria");

                            Double ntGrupoCategoria = (grupoCategoria.getString("ntGrupoCategoria").equals("null") ? 0.0 : grupoCategoria.getDouble("ntGrupoCategoria"));
                            media += ntGrupoCategoria;

                            item.put("idGrupoCategoria", grupoCategoria.getString("id"));
                            item.put("ntGrupoCategoria", ntGrupoCategoria);
                            item.put("nmCategoria", categoria.getString("nmCategoria"));
                            item.put("dsCategoria", categoria.getString("dsCategoria"));

                            listaCategorias.add(item);
                        }
                        media = media / grupoCategoriaList.length();
                        System.out.println("Média = " + media);
                        SimpleAdapter adapterLvCategorias = new SimpleAdapter(
                                getApplicationContext(),
                                listaCategorias,
                                R.layout.lista_categorias_item,
                                new String[]{"ntGrupoCategoria", "nmCategoria", "dsCategoria"},
                                new int[]{R.id.listaCategoriaNtGrupoCategoria, R.id.listaCategoriaNmCategoria, R.id.listaCategoriaDsCategoria});
                        lvCategorias.setAdapter(adapterLvCategorias);
                    } else {
                        tvAvaliacao.setVisibility(View.GONE);
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    private void carregaItensSpinner() {
        WebServiceHandler.get("listaTemas", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray temas) {
                try {
                    listaSpinner = new ArrayList<>();
                    List<String> listaSimples = new ArrayList<>();
                    listaSimples.add("");
                    for (int i = 0; i < temas.length(); i++) {
                        HashMap<String, Object> item = new HashMap<>();
                        JSONObject tema = (JSONObject) temas.get(i);
                        item.put("idTema", tema.getInt("id"));
                        item.put("nmTema", tema.getString("nmTema"));

                        listaSpinner.add(item);
                        listaSimples.add(tema.getString("nmTema"));
                    }
                    String[] array = new String[listaSpinner.size()];
                    array = listaSimples.toArray(array);

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(
                            getApplicationContext(),
                            R.layout.spinner_tema_item,
                            R.id.textView,
                            array);
                    spinner.setAdapter(adapter);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public void salvar(View view) {

        RequestParams params = new RequestParams();
        params.put("idGrupo", idGrupo);
        params.put("idTema", idTema);

        WebServiceHandler.post("salvarGrupo", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Toast.makeText(getApplicationContext(), "Grupo salvo com sucesso!", Toast.LENGTH_SHORT).show();
//                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), responseString, Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void cancelar(View view) {
        finish();
    }
}
