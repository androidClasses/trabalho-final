package edu.viniciussdsilva.trabalhofinal;

import com.loopj.android.http.*;

/**
 * Created by viniciussdsilva on 01/07/2018.
 */

public class WebServiceHandler {

    private static final String BASE_URL = "http://10.0.0.2:8080/";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

}
